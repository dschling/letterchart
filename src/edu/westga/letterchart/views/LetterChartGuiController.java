package edu.westga.letterchart.views;

import java.io.File;
import java.io.IOException;

import edu.westga.letterchart.model.CharacterHistogram;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.PieChart;
import javafx.scene.control.MenuItem;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

/**
 * Controller class for the LetterChartGui
 *
 * @author jrucker2
 *
 */
public class LetterChartGuiController {

	private CharacterHistogram characterHistogram = new CharacterHistogram();

	@FXML
    private MenuItem openMenuButton;

    @FXML
    private MenuItem closeMenuButton;

    @FXML
    private PieChart thePieChart;



    /**
	 * Initializes the GUI components
	 */
    @FXML
    void initialize() {
    	assert this.closeMenuButton != null : "fx:id=\"closeMenuButton\" was not injected: check your FXML file 'LetterChartGui.fxml'.";
    	assert this.openMenuButton != null : "fx:id=\"openMenuButton\" was not injected: check your FXML file 'LetterChartGui.fxml'.";
    	assert this.thePieChart != null : "fx:id=\"thePieChart\" was not injected: check your FXML file 'LetterChartGui.fxml'.";

    	this.setEventActions();

    }

    /**
	 * Handles the events that occur due to interaction with menu items.
	 */
    void setEventActions() {
    	this.closeMenuButton.setOnAction(event -> Platform.exit());
    	this.openMenuButton.setOnAction(event -> this.handleLoadAction());
    }

    private FileChooser initializeFileChooser(String title) {
		FileChooser chooser = new FileChooser();
		chooser.getExtensionFilters().add(
						new ExtensionFilter("Text Files", "*.txt"));
		chooser.setTitle(title);
		return chooser;
	}

    private void handleLoadAction() {
		FileChooser chooser = this.initializeFileChooser("Read from");

		File inputFile = chooser.showOpenDialog(null);
		if (inputFile == null) {
			return;
		}

		try {
			this.characterHistogram.load(inputFile);
			ObservableList<PieChart.Data> dataProperty = this.characterHistogram.dataProperty();
	    	this.thePieChart.setData(dataProperty);

		} catch (IOException readException) {

		}
	}

}
