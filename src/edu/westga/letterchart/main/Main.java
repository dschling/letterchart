/**
 *
 */
package edu.westga.letterchart.main;

import java.net.URL;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Main extends the JavaFX Application class to build the GUI
 * and start program execution.
 *
 * @author jrucker2
 *
 */
public class Main extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		URL resource = classLoader.getResource("edu/westga/letterchart/views/LetterChartGui.fxml");
		FXMLLoader loader = new FXMLLoader(resource);
		Parent root = (Parent) loader.load();
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	/**
	 * Runs application
	 *
	 * @param args launch parameter
	 */
	public static void main(String[] args) {
		launch(args);

	}

}
