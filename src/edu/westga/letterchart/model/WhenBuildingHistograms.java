package edu.westga.letterchart.model;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

/**
 * Test Class
 * @author dschlin1
 *
 */
public class WhenBuildingHistograms {

	private CharacterHistogram hist;
	private char[] alphabet = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
	private char[] punct = new char[] { '!', '@', '#', '$', '%', '^', '(', ')', '-', '_', '=', '+', ';', ':', '"', ',', '.', '/', '<', '>', '?' };

	/**
	 * constructor
	 */
	public WhenBuildingHistograms() {
		this.hist = new CharacterHistogram();
	}

	/**
	 * Set up
	 */
	@Before
	public void setUp() {
		this.load(new File("src/edu/westga/letterchart/model/Empty.txt"));
	}

	private void load(File textFile) {
		try {
			this.hist.load(textFile);
		} catch (IOException exc) {
			exc.printStackTrace();
		}
	}

	/**
	 * Test Method
	 */
	@Test(expected = IllegalArgumentException.class)
	public void nullTextFileShouldThrowException() {
		try {
			this.hist.load(null);
		} catch (IOException exc) {
			exc.printStackTrace();
		}
	}

	/**
	 * Test Method
	 */
	@Test
	public void emptyFileShouldHaveNoData() {
		for (char character : this.alphabet) {
			assertEquals(0, this.hist.getCountFor(character));
		}
		for (char character : this.punct) {
			assertEquals(0, this.hist.getCountFor(character));
		}
	}

	/**
	 * Test Method
	 */
	@Test
	public void singleLetterFileShouldHaveOneEntryWithCountOfOne() {
		this.load(new File("src/edu/westga/letterchart/model/SingleLetter.txt"));
		for (char character : this.alphabet) {
			if (character == 'b') {
				assertEquals(1, this.hist.getCountFor(character));
			} else {
				assertEquals(0, this.hist.getCountFor(character));
			}
		}
		for (char character : this.punct) {
			assertEquals(0, this.hist.getCountFor(character));
		}
	}

	/**
	 * Test Method
	 */
	@Test
	public void fileWithOneRepeatingLetterShouldOnlyCountThatLetter() {
		this.load(new File("src/edu/westga/letterchart/model/RepeatingLetter.txt"));
		assertEquals(10, this.hist.getCountFor('g'));
	}

	/**
	 * Test Method
	 */
	@Test
	public void fileWithOneOfEachLetterShouldHaveAppropriateHistogram() {
		this.load(new File("src/edu/westga/letterchart/model/Alphabet.txt"));
		for (char character : this.alphabet) {
			assertEquals(1, this.hist.getCountFor(character));
		}
		for (char character : this.punct) {
			assertEquals(0, this.hist.getCountFor(character));
		}
	}

	/**
	 * Test Method
	 */
	@Test
	public void fileWithManyOfSomeLettersShouldHaveAppropriateHistogram() {
		this.load(new File("src/edu/westga/letterchart/model/HelloWorld.txt"));
		assertEquals(1, this.hist.getCountFor('h'));
		assertEquals(1, this.hist.getCountFor('e'));
		assertEquals(3, this.hist.getCountFor('l'));
		assertEquals(2, this.hist.getCountFor('o'));
		assertEquals(1, this.hist.getCountFor('w'));
		assertEquals(1, this.hist.getCountFor('r'));
		assertEquals(1, this.hist.getCountFor('d'));
		for (char character : this.punct) {
			assertEquals(0, this.hist.getCountFor(character));
		}
	}

	/**
	 * Test Method
	 */
	@Test
	public void fileWithOnlyPunctuationShouldHaveNoData() {
		this.load(new File("src/edu/westga/letterchart/model/Punctuation.txt"));
		for (char character : this.alphabet) {
			assertEquals(0, this.hist.getCountFor(character));
		}
		for (char character : this.punct) {
			assertEquals(0, this.hist.getCountFor(character));
		}
	}

	/**
	 * Test Method
	 */
	@Test
	public void fileWithMixedLettersAndPunctuationShouldIgnorePunctuation() {
		this.load(new File("src/edu/westga/letterchart/model/Mixed.txt"));
		assertEquals(1, this.hist.getCountFor('h'));
		assertEquals(1, this.hist.getCountFor('e'));
		assertEquals(3, this.hist.getCountFor('l'));
		assertEquals(2, this.hist.getCountFor('o'));
		assertEquals(1, this.hist.getCountFor('w'));
		assertEquals(1, this.hist.getCountFor('r'));
		assertEquals(1, this.hist.getCountFor('d'));
		for (char character : this.punct) {
			assertEquals(0, this.hist.getCountFor(character));
		}
	}

}
