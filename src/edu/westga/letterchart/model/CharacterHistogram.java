package edu.westga.letterchart.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.PieChart.Data;

/**
 * A CharacterHistogram is a histogram of letter values
 * read from a text file.
 * @author lewisb
 *
 */
public class CharacterHistogram {

	private Map<Character, Integer> characterList;

	/**
	 * Creates an empty CharacterHistogram.
	 */
	public CharacterHistogram() {
		this.characterList = new HashMap<Character, Integer>();

	}

	/**
	 * Gets the count for a given character.
	 *
	 * @param character the character we're interested in
	 * @return the number of times it appears in the histogram
	 */
	public int getCountFor(char character) {
		if (!this.characterList.containsKey(character)) {
			return 0;
		}
		return this.characterList.get(character);
	}

	/**
	 * Increases the count for the specified character, ignoring
	 * punctuation.
	 * @param character	char to increase count for
	 */
	private void increaseCountFor(char character) {
		if (Character.isAlphabetic(character)) {
			character = Character.toLowerCase(character);
			Integer value = 1;
			if (this.characterList.containsKey(character)) {
				value = this.characterList.get(character);
				value++;
			}
			this.characterList.put(character, value);
		}
	}

	/**
	 * Clears this CharacterHistogram and loads in the new character data
	 *
	 * @param textFile the file to open
	 * @throws FileNotFoundException if the file cannot be found
	 * @throws IOException if an unexpected I/O problem occurs
	 * @throws IllegalArgumentException if the file is null
	 */
	public void load(File textFile) throws FileNotFoundException, IOException {
		if (textFile == null) {
			throw new IllegalArgumentException("The text file to load is null");
		}
		this.characterList.clear();
		String line;
		BufferedReader reader = new BufferedReader(new FileReader(textFile));
		while ((line = reader.readLine()) != null) {
			char[] charArray = line.toCharArray();
			for (int i = 0; i < charArray.length; i++) {
				this.increaseCountFor(charArray[i]);
			}
		}
		reader.close();
		System.out.println(this.characterList.toString());
	}

	/**
	 * Data property, suitable for consumption by a PieChart.
	 *
	 * @return the histogram as pie char data
	 */
	public ObservableList<PieChart.Data> dataProperty() {
		ObservableList<PieChart.Data> observableList = FXCollections.observableArrayList();
		for (Map.Entry<Character, Integer> entry : this.characterList.entrySet()) {
			PieChart.Data data = new Data(entry.getKey().toString(), entry.getValue());
			observableList.add(data);
		}
		return observableList;
	}
}
